package menu;

import openfl.Assets;
import openfl.display.Bitmap;
import openfl.system.System;

import common.State;

import ui.ImageButton;

class MenuMain extends State {
  var playButton: ImageButton;
  var quitButton: ImageButton;
  var logo: Bitmap;

  public function new() {
    super();

    logo = new Bitmap(Assets.getBitmapData('assets/ui/menu/logo.png'));
    logo.y = 50;
    logo.x = (1920 - logo.width) / 2;
    addChild(logo);

    playButton = new ImageButton('assets/ui/menu/play.png', function() {
      Main.instance.setState(new game.GameMain());
    });
    playButton.x = 1920 / 2;
    playButton.y = 1080 / 2;
    addChild(playButton);

    quitButton = new ImageButton('assets/ui/menu/quit.png', function() {
      System.exit(0);
    });
    quitButton.x = 1920 / 2;
    quitButton.y = 1080 - quitButton.height - 50;
    addChild(quitButton);
  }
}
