package;

import openfl.display.Sprite;
import openfl.Lib;
import openfl.display.StageDisplayState;
import openfl.system.Capabilities;

import common.State;
import common.StateMachine;

import game.GameMain;

class Main extends StateMachine {
  public function new () {
    super();

    State.target_width = 192;
    State.target_height = 108;

    var config = Config.get();

    Lib.application.window.resize(config.window.width, config.window.height);

    var posX: Int = Std.int((Capabilities.screenResolutionX - config.window.width) / 2);
    var posY: Int = Std.int((Capabilities.screenResolutionY - config.window.height) / 2);

    Lib.application.window.move(posX, posY);

    if(config.window.fullscreen) {
      Lib.current.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
    }

    setState(new GameMain());
  }
}
