package;

import openfl.Assets;

typedef ConfigStruct = {
  window: {
    width: Int,
    height: Int,
    fullscreen: Bool
  }
};

class Config {
  public static function get():ConfigStruct {
    return haxe.Json.parse(Assets.getText('config/config.json'));
  }
}
