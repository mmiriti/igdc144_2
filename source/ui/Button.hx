package ui;

import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.Lib;

class Button extends Sprite {
  var action: Void -> Void;

  public function new(action: Void -> Void) {
    super();

    this.action = action;

    buttonMode = true;

    addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
    addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
    addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
  }

  function onMouseOver(e: MouseEvent) {
    scaleX = scaleY = 1.05;
  }

  function onMouseOut(e: MouseEvent) {
    scaleX = scaleY = 1;
  }

  function onMouseDown(e: MouseEvent) {
    if(action != null) {
      action();
    }

    Lib.current.stage.focus = Lib.current.stage;
  }
}
