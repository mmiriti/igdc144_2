package ui;

import openfl.display.Bitmap;
import openfl.Assets;

class ImageButton extends Button {
  var bitmap: Bitmap;

  public function new(image: String, action: Void -> Void) {
    super(action);

    bitmap = new Bitmap(Assets.getBitmapData(image));
    bitmap.x = -bitmap.width / 2;
    bitmap.y = -bitmap.height / 2;
    addChild(bitmap);
  }
}
