package common;

import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;

typedef AnimationFrame = {
  bitmapData: BitmapData,
  duration: Float
}

typedef AnimationTag = {
  name: String,
  from: Int,
  to: Int,
  direction: String
}

class Anim extends Bitmap {
  public var currentFrame(default, set): Int;
  public var playTag(default, set): String;
  public var fps: Int = 25;
  public var reverse: Bool = false;

  private var frames:Array<AnimationFrame> = [];
  private var lastTime: Float = 0;
  private var frameTime: Float = 0;

  private var tag: AnimationTag = null;
  private var tags: Map<String, AnimationTag> = new Map<String, AnimationTag>();

  private var direction: String = 'forward';

  public function new () {
    super();
    addEventListener(Event.ENTER_FRAME, onEnterFrame);
  }

  public function simple(bitmapData: BitmapData, frameWidth: Int, frameHeight: Int, fps: Int = 25) {
    this.fps = fps;

    for(n in 0...Std.int(bitmapData.width / frameWidth)) {
      var frame = new BitmapData(frameWidth, frameHeight);
      frame.copyPixels(bitmapData, new Rectangle(n * frameWidth, 0, frameWidth, frameHeight), new Point(0, 0));
      frames.push({
        bitmapData: frame,
        duration: 1000 / fps
      });
    }

    currentFrame = 0;
  }

  function onEnterFrame(e: Event) {
    if(lastTime != 0) {
      var currentTime = Lib.getTimer(); 
      var delta = currentTime - lastTime;

      frameTime += delta;

      while(frameTime >= frames[currentFrame].duration) {
        currentFrame += (direction == 'forward' ? 1 : -1) * (reverse ? -1 : 1);
        frameTime = frameTime - frames[currentFrame].duration;
      }

      lastTime = currentTime;
    } else {
      lastTime = Lib.getTimer();
    }
  }

  /**
    Enter frame
    **/
  function set_currentFrame(frame: Int): Int {
    if(tag.direction == 'pingpong') {
      if(frame > tag.to) {
        frame = tag.to - 1;
        direction = direction == 'forward' ? 'backwards' : 'forward';
      }

      if(frame < tag.from) {
        frame = tag.from + 1;
        direction = direction == 'forward' ? 'backwards' : 'forward';
      }
    } else {
      if(frame > tag.to) {
        frame = tag.from;
      }

      if(frame < tag.from) {
        frame = tag.to - 1;
      }
    }

    bitmapData = frames[frame].bitmapData;

    return currentFrame = frame;
  }

  public function fromJSON(json: String, bitmapData: BitmapData) {
    var data = haxe.Json.parse(json);

    var jframes: Array<Dynamic> = data.frames;

    for(jframe in jframes) {
      var bmp = new BitmapData(
          jframe.frame.w, 
          jframe.frame.h
          );

      bmp.copyPixels(bitmapData, new Rectangle(
            jframe.frame.x, 
            jframe.frame.y, 
            jframe.frame.w, 
            jframe.frame.h
            ), new Point(0, 0));

      frames.push({
        bitmapData: bmp,
        duration: Std.parseInt(jframe.duration)
      });
    }

    tags.set('[full]', {
      name: '[full]',
      from: 0,
      to: frames.length - 1,
      direction: 'forward'
    });

    var frameTags: Array<Dynamic> = data.meta.frameTags;

    for(ftag in frameTags) {
      tags.set(ftag.name, ftag);
    }

    playTag = '[full]';
  }

  function set_playTag(value: String) {
    if(tags.exists(value)) {
      playTag = value;

      tag = tags.get(playTag);
      direction = 'forward';
      currentFrame = tag.from;
    }

    return playTag;
  }
}
