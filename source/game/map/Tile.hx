package game.map;

import openfl.display.Bitmap;

import common.GameSprite;

class Tile extends GameSprite {
  public function new(tileData: TileData) {
    super();
    var bitmap:Bitmap = new Bitmap(tileData.bitmapData);
    addChild(bitmap);
  }
}
