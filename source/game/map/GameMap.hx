package game.map;

import haxe.xml.Fast;

import openfl.Assets;

import common.GameSprite;

import game.mobs.Player;

class GameMap extends GameSprite {
  public var tileSet: Tileset;

  var tiles: Array<Array<Array<Tile>>> = [];

  public function new() {
    super();
  }

  public static function getMap(mapName: String): GameMap {
    var newMap = new GameMap();
    newMap.load(mapName);
    return newMap;
  }

  public function load(mapName: String) {
    var xml: Xml = Xml.parse(Assets.getText('maps/' + mapName + '.tmx'));
    var fast: Fast = new Fast(xml.firstElement());
    var layer_index: Int = 0;

    tileSet = new Tileset(fast.node.tileset);

    for (layer in fast.nodes.layer) {
      var tileX:Int = 0;
      var tileY:Int = 0;

      // var layerWidth: Int = Std.int(layer.att.width);
      // var layerHeight: Int = Std.int(layer.att.height);

      // tiles.push([for (i in 0...layerWidth) [for (j in 0...layerHEightkj) null]]);

      for(tile in layer.node.data.nodes.tile) {
        var tile = tileSet.getTile(Std.parseInt(tile.att.gid));
        if(tile != null) {
          tile.x = tileX * tileSet.tilewidth;
          tile.y = tileY * tileSet.tileheight;

          // tiles[layer_index][tileX][tileY] = tile;

          addChild(tile);
        }

        tileX++;

        if(tileX >= Std.parseInt(layer.att.width)) {
          tileX = 0;
          tileY++;
        }
      }
      layer_index++;
    }

    for(object in fast.node.objectgroup.nodes.object) {
      var b_x:Int = 0;
      var b_y:Int = 0;
      var b_width:Int = 0;
      var b_height:Int = 0;
      var properties: Map<String, String> = new Map<String, String>();

      if((object.has.x) && (object.has.y)) {
        b_x = Std.parseInt(object.att.x);
        b_y = Std.parseInt(object.att.y);
      }

      if((object.has.width) && (object.has.height)) {
        b_width = Std.parseInt(object.att.width);
        b_height = Std.parseInt(object.att.height);
      }

      for(property in object.node.properties.nodes.property) {
        properties.set(property.att.name, property.att.value);
      }

      if(object.att.type == 'mob') {
        if(properties.get('type') == 'player') {
          var player = Player.getInstance();
          player.x = b_x + b_width / 2;
          player.y = b_y + b_height / 2;
          addChild(player);
        }
      }
    }
  }
}
