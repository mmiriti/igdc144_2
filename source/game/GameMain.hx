package game;

import openfl.ui.Keyboard;

import common.State;
import common.GameSprite;

import game.map.GameMap;
import game.mobs.Player;

class GameMain extends State {
  var _actionLayer: GameSprite = new GameSprite();

  public function new() {
    super();

    _actionLayer.addChild(GameMap.getMap('test'));

    addChild(_actionLayer);
  }

  override function update(delta: Float) {
    super.update(delta);

    _actionLayer.x = -Player.getInstance().x + State.target_width / 2;
    _actionLayer.y = -Player.getInstance().y + State.target_height / 2;
  }

  override function keyDown(keyCode: Int) {
    Player.getInstance().keyDown(keyCode);
  }

  override function keyUp(keyCode: Int) {
    Player.getInstance().keyUp(keyCode);
  }
}
