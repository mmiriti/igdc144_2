package game.mobs;

import common.GameSprite;

class Mob extends GameSprite {
  public function new() {
    super();

    graphics.beginFill(0xff0000);
    graphics.drawRect(-4, -4, 8, 8);
    graphics.endFill();
  }
}
