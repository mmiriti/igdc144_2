package game.mobs;

import openfl.ui.Keyboard;

import motion.Actuate;

import common.Vector;

class Player extends Mob {
  private static var _instance: Player;

  public var speed: Vector = new Vector(100, 100);
  var move: Vector = new Vector();

  var isLeft: Bool = false;
  var isRight: Bool = false;
  var isUp: Bool = false;
  var isDown: Bool = false;

  public function new() {
    super();
  }

  override function update(delta: Float) {
    super.update(delta);

    x += move.x * speed.x * delta;
    y += move.y * speed.y * delta;
  }

  public static function getInstance(create: Bool = false): Player {
    if((_instance == null) || (create)) {
      _instance = new Player();
    }

    return _instance;
  }

  public function keyDown(keyCode: Int) {
    if(keyCode == Keyboard.W) {
      isUp = true;
    }

    if(keyCode == Keyboard.S) {
      isDown = true;
    }

    if(keyCode == Keyboard.A) {
      isLeft = true;
    }

    if(keyCode == Keyboard.D) {
      isRight = true;
    }

    updateMove();
  }

  public function keyUp(keyCode: Int) {
    if(keyCode == Keyboard.W) {
      isUp = false;
    }

    if(keyCode == Keyboard.S) {
      isDown = false;
    }

    if(keyCode == Keyboard.A) {
      isLeft = false;
    }

    if(keyCode == Keyboard.D) {
      isRight = false;
    }

    updateMove();
  }

  function updateMove() {
    move.x = (isLeft ? -1 : 0) + (isRight ? 1 : 0);
    move.y = (isUp ? -1 : 0) + (isDown ? 1 : 0);
  }
}
